# Evaluation SQL 2 Julick Mellah

Après avoir cloné le projet, vous pouvez créer le conteneur docker de l'environnement mySql en exécutant la commande suivante dans le répertoire racine du projet:

```bash
docker compose up
```

Ensuite vous pouvez vous connecter à la base de données depuis n'importe quel client SQL en utilisant les informations suivantes:

```
identifiant: julick
mot de passe: admin
port: 3306
nom de la base de données: bus
host: 127.0.0.1
```

Par la suite vous pouvez exécuter les scripts SQL pour créer les tables et insérer les données dans la base de données.

Dans un premier temps passez le contenu du script `schema.sql` dans votre client SQL pour créer la base de données et les tables.

Ensuite passez le contenu du script `data.sql` pour insérer les données dans les tables.

Après, vous pouvez exécuter les requêtes SQL du fichier `queries.sql` pour répondre aux questions de l'évaluation.

Voici le mcd de la base de données:

<img src="mcd.png">