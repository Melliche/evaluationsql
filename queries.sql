-- 1
SELECT Horaire.Heure_Passage AS "Horaires à l'arrêt Madeleine"
FROM
    Arret
    JOIN Arret_Horaire ON Arret.ID_Arret = Arret_Horaire.ID_Arret
    JOIN Horaire ON Arret_Horaire.ID_Horaire = Horaire.ID_Horaire
WHERE
    Arret.Nom = 'Madeleine';

SELECT Horaire.Heure_Passage AS "Horaires à l'arrêt République"
FROM
    Arret
    JOIN Arret_horaire ON Arret.ID_Arret = Arret_horaire.ID_Arret
    JOIN Horaire ON Arret_horaire.ID_Horaire = Horaire.ID_Horaire
WHERE
    Arret.Nom = 'République';

-- 2
SELECT Arret.Nom AS "Parcours de la ligne 2 Direction Kersec"
FROM
    Arret
    JOIN Arret_Direction ON Arret.ID_Arret = Arret_Direction.ID_Arret
    JOIN Direction ON Arret_Direction.ID_direction = Direction.ID_direction
WHERE
    Direction.Nom = 'Kersec';

-- 4 5
SELECT
    CASE
        WHEN Arret.Nom = 'Petit Tohannic' THEN CONCAT(
            'L\'arrêt n\'est pas desservi. Veuillez vous reporter à l\'arrêt ', Arret_Redirection.Nom, '.'
        )
        ELSE Horaire.Heure_Passage
    END AS "Horaires à l'arrêt Petit Tohannic"
FROM
    Arret
    LEFT JOIN Redirection_Temporaire ON Arret.ID_Arret = Redirection_Temporaire.ID_Arret_Non_Desservi
    LEFT JOIN Arret AS Arret_Redirection ON Redirection_Temporaire.ID_Arret_Redirection = Arret_Redirection.ID_Arret
    LEFT JOIN Arret_horaire ON Arret.ID_Arret = Arret_horaire.ID_Arret
    LEFT JOIN Horaire ON Arret_horaire.ID_Horaire = Horaire.ID_Horaire
WHERE
    Arret.Nom = 'Petit Tohannic';

-- 6 7
SELECT CONCAT('Ligne 2', ' ', Direction.Nom) AS Ligne, GROUP_CONCAT(
        Arret.Nom
        ORDER BY Arret_Direction.order_in_direction SEPARATOR ','
    ) AS "Arrêts desservis"
FROM
    Direction
    LEFT JOIN Arret_Direction ON Direction.ID_direction = Arret_Direction.ID_direction
    LEFT JOIN Arret ON Arret_Direction.ID_Arret = Arret.ID_Arret
GROUP BY
    Direction.ID_direction,
    Direction.Nom;