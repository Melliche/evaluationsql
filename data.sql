USE bus;

-- Suppression des données des tables
DELETE FROM Horaire;

DELETE FROM Arret;

DELETE FROM Direction;

DELETE FROM Ligne;

DELETE FROM Arret_Horaire;

DELETE FROM Ligne_Direction;

DELETE FROM Ligne_Arret;

DELETE FROM Arret_Direction;

-- Insertions des arrets de bus
INSERT INTO
    Arret (ID_Arret, Nom)
VALUES (1, 'P+R Ouest'),
    (2, 'Fourchêne 1'),
    (3, 'Madeleine'),
    (4, 'République'),
    (5, 'PIBS 2'),
    (6, 'Petit Tohannic'),
    (7, 'Delestraint'),
    (8, 'Kersec');

-- Insertions des horaires
INSERT INTO
    Horaire (ID_Horaire, Heure_Passage)
VALUES (1, '6:32:00'),
    (2, '6:42:00'),
    (3, '6:52:00'),
    (4, '7:00:00'),
    (5, '7:10:00'),
    (6, '6:34:00'),
    (7, '6:44:00'),
    (8, '6:54:00'),
    (9, '7:02:00'),
    (10, '7:12:00'),
    (11, '6:37:00'),
    (12, '6:47:00'),
    (13, '6:57:00'),
    (14, '7:06:00'),
    (15, '7:16:00'),
    (16, '7:22:00'),
    (17, '7:07:00'),
    (18, '7:17:00'),
    (19, '6:46:00'),
    (20, '6:56:00'),
    (21, '7:27:00'),
    (22, '6:50:00'),
    (23, '7:11:00'),
    (24, '7:21:00'),
    (25, '7:31:00'),
    (26, '6:51:00'),
    (27, '7:01:00'),
    (28, '7:32:00'),
    (29, '6:55:00'),
    (30, '7:05:00'),
    (31, '7:26:00'),
    (32, '7:36:00');

-- Insertions des lignes de bus
INSERT INTO Ligne (ID_Ligne, Nom) VALUES (1, 'Ligne 2');

-- Insertions des Directions
INSERT INTO
    Direction (ID_Direction, Nom)
VALUES (1, 'P+R Ouest'),
    (2, 'Kersec');

INSERT INTO
    Arret_Horaire (ID_Arret, ID_Horaire)
VALUES (3, 11),
    (3, 12),
    (3, 13),
    (3, 14),
    (3, 15),
    (4, 2),
    (4, 3),
    (4, 9),
    (4, 10),
    (4, 16);

INSERT INTO
    Arret_Direction (
        ID_Arret, ID_Direction, Order_in_direction
    )
VALUES (1, 1, 1),
    (2, 1, 2),
    (3, 1, 3),
    (4, 1, 4),
    (5, 1, 5),
    (6, 1, 6),
    (7, 1, 7),
    (8, 1, 8),
    (8, 2, 1),
    (7, 2, 2),
    (6, 2, 3),
    (5, 2, 4),
    (4, 2, 5),
    (3, 2, 6),
    (2, 2, 7),
    (1, 2, 8);

INSERT INTO
    Redirection_Temporaire (
        ID_Arret_Non_Desservi, ID_Arret_Redirection
    )
VALUES (6, 7);